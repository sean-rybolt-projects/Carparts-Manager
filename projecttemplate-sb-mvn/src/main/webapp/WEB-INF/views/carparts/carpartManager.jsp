<%@ include file="/WEB-INF/layouts/include.jsp"%>



<div>
	<h1>Carpart Manager</h1>
	<a class="btn btn-primary" href="/carparts/partnumber" role="button">Add Part</a>

			<div id="myTable">
				<orly-table id="orly-table" url="<c:url value='/carparts/collectCarparts'/>" loaddataoncreate>
					<orly-column label="Action" class="">
					    <div slot="cell">
					    	<a href="">
					      		<orly-icon name="edit" color="orange" class="editBtn" spinonclick data-id=\${model.id}></orly-icon>
					      	</a>
					      	<a href="">
					      		<orly-icon name="trash-2" color="tomato" class="deleteBtn" spinonclick data-id=\${model.id}></orly-icon>
					      	</a>
					    </div>
					  </orly-column>
					<orly-column field="partNumber" label="Part Number"></orly-column>
					<orly-column field="title" label="Title"></orly-column>
					<orly-column field="line" label="Line"></orly-column>
					<orly-column field="description" label="Description"></orly-column>

				</orly-table>
			</div>				
</div>

<script>
orly.ready.then(function(){
	orly.on(myTable, "click", (e) =>{
		let button = e.target;
		if(button.classList.contains("deleteBtn")){
			let td = button.parentNode.parentNode;
			let partNumRow = td.parentNode.nextSibling;
			let partNum = partNumRow.lastElementChild.innerText;
			let mylink = button.parentNode;
			location.href = ("http://localhost:8080/carparts/delete/" + partNum);
			
		}
		else if(button.classList.contains("editBtn")){
			let td = button.parentNode.parentNode;
			let partNumRow = td.parentNode.nextSibling;
			let partNum = partNumRow.lastElementChild.innerText;
			let mylink = button.parentNode;
			location.href = ("http://localhost:8080/carparts/partnumber/"+ partNum);
			
		}
		else{
			console.log("Wrong");
		}
		
		
	})
});

</script>