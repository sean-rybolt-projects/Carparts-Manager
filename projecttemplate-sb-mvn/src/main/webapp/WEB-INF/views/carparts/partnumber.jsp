<%@ include file="/WEB-INF/layouts/include.jsp"%>

<script src="resources/js/oreillyjs/1/orly.js"></script>
<link rel="stylesheet" href="resources/css/oreillybs-4.0.0r1.min.css" />

<h1>Add New Car Part</h1>
<div class="row">
	<div class="col-sm-12">
		<form is="orly-form" id="form" method="post" action="<c:url value='/carparts/partnumber' />">
			<c:if test="${not empty carpart}">
			    <input type="hidden" id="update" name="update" value="true" />
			</c:if>
			<div class="col-sm-4 form-group">
			  <label for="partnumber">Part #</label>
			  <orly-input id="partnumber" name="partnumber" value="${carpart.partNumber}" placeholder="Part #"></orly-input>
			</div>
			<div class="col-sm-4 form-group">
			  <label for="title">Title</label>
			  <orly-input id="title" name="title" value="${carpart.title}"  placeholder="Title"></orly-input>
			</div>
			<div class="col-sm-4 form-group">
			  <label for="title">Line</label>
			  <orly-input id="line" name="line" value="${carpart.line}"  placeholder="Line"></orly-input>
			</div>
			<div class="col-sm-4 form-group">
			  <label for="title">Description</label>
			  <orly-input id="description" name="description" value="${carpart.description}"  placeholder="Description"></orly-input>
			</div>
			<div class="col-sm-4 form-group">
			  <!-- <orly-btn id="submitBtn" type="submit" spinonclick text="Submit"></orly-btn> -->
			  <button id="submitBtn" type="submit" class="btn btn-primary">Submit</button>

			</div>
		</form>
	</div>
</div>



