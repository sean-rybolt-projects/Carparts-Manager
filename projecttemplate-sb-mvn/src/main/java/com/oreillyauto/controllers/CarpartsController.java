package com.oreillyauto.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.oreillyauto.domain.carparts.Carpart;
import com.oreillyauto.service.CarpartsService;

@Controller
public class CarpartsController {

	@Autowired
	CarpartsService carpartsService;

	//Controller for Add part Button
    @GetMapping(value = { "/carparts/partnumber" })
    public String getPartnumber(Model model, String partnumber) throws Exception {
        if (partnumber != null && partnumber.length() > 0) {
            Carpart carpart = carpartsService.getCarpartByPartNumber(partnumber);
            model.addAttribute("carpart", carpart);
        }

        model.addAttribute("active", "add");
        return "partnumber";
    }
    //Post method for submit on update/add part
    @PostMapping(value = { "/carparts/partnumber" })
    public String postPartnumber(Model model, String partnumber, String title, String update, String description, String line) {
        Carpart carpart = new Carpart();
        carpart.setPartNumber(partnumber);
        carpart.setTitle(title);
        carpart.setLine(line);
        carpart.setDescription(description);        
        carpartsService.saveCarpart(carpart);
        boolean saved = true;
        
        String action = ("true".equalsIgnoreCase(update)) ? "Updated" : "Saved";
        
        if (saved) {
            model.addAttribute("messageType", "success");
            model.addAttribute("message", "Part Number " + partnumber + " "+action+" Successfully");    
            model.addAttribute("carpart", carpart);
        } else {
            model.addAttribute("messageType", "danger");
            model.addAttribute("message", "Part Number " + partnumber + 
                    " Not " + action + " Successfully!");               
        }

        return "carpartManager";
    }
    

   
    //Controller for deleting part from DB
    @GetMapping(value = { "/carparts/delete/{partnumber}" })
    public String deleteCarpart(@PathVariable String partnumber, Model model) {
        Carpart carpart = new Carpart();
        if (partnumber != null && partnumber.length() > 0) {
            try {
                carpart = carpartsService.getCarpartByPartNumber(partnumber);
            }
            catch(Exception e) {
                System.out.println(e.getStackTrace());
            }
            carpartsService.deleteCarpartByPartNumber(carpart);
            model.addAttribute("message", "Car Part with part # = " + partnumber + " Deleted Successfully");
        }
        
        return "carpartManager";
    }
    
    //Controller that allows for updating a part
    @GetMapping(value = { "/carparts/partnumber/{partnumber}" })
    public String getPartnumber(@PathVariable String partnumber, Model model) throws Exception {
        if (partnumber != null && partnumber.length() > 0) {
            Carpart carpart = carpartsService.getCarpartByPartNumber(partnumber);
            model.addAttribute("carpart", carpart);
        }

        model.addAttribute("active", "add");
        return "partnumber";
    }


    //Controller that handles head light link
	@GetMapping(value = {"/carparts/electrical/{partNumber}"})
	public String getElectrical(@PathVariable String partNumber, Model model) throws Exception {
	      String error = "";
	        Carpart carpart = carpartsService.getCarpartByPartNumber(partNumber);
	        
	        if (carpart == null) {
	            error = "Sorry, cannot find part number " + partNumber;
	        }
	            
	        model.addAttribute("carpart", ((carpart == null) ? new Carpart() : carpart));
	        model.addAttribute("active", partNumber);
	        model.addAttribute("error", error);
	        return "carparts";
	}
	
	//Controller that handles Oil pump link
	@GetMapping(value = {"/carparts/engine/{partNumber}"})
	public String getEngine(@PathVariable String partNumber, Model model) throws Exception {
        String error = "";
        Carpart carpart = carpartsService.getCarpartByPartNumber(partNumber);
        
        if (carpart == null) {
            error = "Sorry, cannot find part number " + partNumber;
        }
            
        model.addAttribute("carpart", ((carpart == null) ? new Carpart() : carpart));
        model.addAttribute("active", partNumber);
        model.addAttribute("error", error);
        return "carparts";
	}
	
	//Controller that handles air pump link
	@GetMapping(value = {"/carparts/other/{partNumber}"})
	public String getOther(@PathVariable String partNumber, Model model) throws Exception {
        String error = "";
        Carpart carpart = carpartsService.getCarpartByPartNumber(partNumber);
        
        if (carpart == null) {
            error = "Sorry, cannot find part number " + partNumber;
        }
            
        model.addAttribute("carpart", ((carpart == null) ? new Carpart() : carpart));
        model.addAttribute("active", partNumber);
        model.addAttribute("error", error);
        return "carparts";
	}

	//Controller for the page containing Orly Table
	@GetMapping(value = { "/carparts/carpartManager" })
	public String getCarpartManager(Model model) {
	      model.addAttribute("active", "manage");
	      return "carpartManager";
	}
	
	@GetMapping(value = { "/carparts" })
	public String carparts(Model model, String firstName, String lastName) throws Exception {


		return "carparts";
	}
	
	//Method to retrieve JSON data for the Orly Table
	@GetMapping("/carparts/collectCarparts")
	@ResponseBody
	public List<Carpart> collectCarparts(Model model) {
	    List<Carpart> carpartList = carpartsService.getCarparts();
	    return carpartList;
	}
	
	

}
