package com.oreillyauto.service.impl;

import java.util.Collections;
import java.util.List;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.oreillyauto.dao.CarpartsRepository;
import com.oreillyauto.domain.carparts.Carpart;
import com.oreillyauto.service.CarpartsService;

@Service("carpartsService")
public class CarpartServiceImpl implements CarpartsService {
	@Autowired
	CarpartsRepository carpartsRepo;
	
	//All methods use CRUDRepo methods to do their respective actions
	@Override
	public void saveCarpart(Carpart carpart) {
	    carpartsRepo.save(carpart);
	    
	}
	

    @Override
    public Carpart getCarpartByPartNumber(String partNumber) throws Exception {    	
        return carpartsRepo.getCarpart(partNumber);
    }


	@Override
	public List<Carpart> getCarparts() {
	    return (List<Carpart>)carpartsRepo.findAll();
	}


	@Override
	public void deleteCarpartByPartNumber(Carpart carpart) {
	    carpartsRepo.delete(carpart);
	}

}
